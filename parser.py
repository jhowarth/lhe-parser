#!/usr/bin/env python3
# Python modules
import argparse
import xml.etree.ElementTree as ET
import sys
from particles import Particle


###--- Argument parser setup ---###
parser = argparse.ArgumentParser()
parser.add_argument("--infile",  
                    help    = "File in LHE format to be read",  
                    nargs   = '?', 
                    type    = str, 
                    default = "random.lhe")
parser.add_argument("--outfile", 
                    help    = "File in ROOT format for output", 
                    nargs   = '?', 
                    type    = str, 
                    default = "test.root")
parser.add_argument("--outtree", 
                    help    = "output TTree name",              
                    nargs   = '?', 
                    type    = str, 
                    default = "test")
parser.add_argument("--cme",
                    help    = "Centre of mass energy in MEV, default is 13 TeV",
                    nargs   = '?',
                    type    = float,
                    default = 13000)
parser.add_argument("--particles",
                    help    = "Force the script to add particles",
                    nargs   = '?',
                    type    = str,
                    default = '')
parser.add_argument("--generator",
                    help    = "Tell the code which generator made the LHE file [madgraph/superchic/powheg/herwig/pythia]",
                    nargs   = '?',
                    type    = str,
                    default = '')

args = parser.parse_args()

###-- Need to import root afterwards or it screws up the --help option
from ROOT import TTree, TFile, vector, gROOT, TLorentzVector


def ResetBranches(branches):

    for branch in branches:
        branch.resize(0)


def BookBranch(name, suffix, tree, branches):

    """ Function to book branches to an output TTree """

    branch_type = 'null'
    if "F" in suffix:
        branch_type = 'float'
    elif "I" in suffix:
        branch_type = 'int'
    else:
        print("WARNING: Type not recognised",suffix)

    branch = vector(branch_type)(20)
    tree.Branch(name, branch)
    branches.append(branch)
    return branch


def convertElementToList(element):

    """ Converts an elementTree element into a list of strings """

    returnList = element.text.split("\n")
    returnList = filter(None, returnList)
    returnList = list(returnList)
    return returnList


def checkGenerator(line, name):

    """ Checks to see if a generators name appears in a line of text """

    if name.lower() in line.lower():
        print("LHEparser: This appears to be an LHE made by " + name)
        return True
    else:
        return False


def main():

    print("LHEparser: Welcome to the LHE Parser, attempting to parse input file")
    print("LHEparser: (this may take a while if you have a lot of events)")    
    assert not ".lhe.gz" in args.infile, "You need to un-tar the input file first"

    LHEeventlist = ET.parse(args.infile)
    root         = LHEeventlist.getroot()
    eventlist    = []

    ###--- Try to work out what generator this is (in order to understand the weights) --###
    isPythia    = False
    isHerwig    = False
    isMadGraph  = False
    isPowheg    = False
    isSuperChic = False

    if (args.generator).lower() == "pythia":
        isPythia    = True
    if (args.generator).lower() == "herwig":
        isHerwig    = True
    if (args.generator).lower() == "madgraph":
        isMadGraph  = True
    if (args.generator).lower() == "powheg":
        isPowheg    = True
    if (args.generator).lower() == "superchic":
        isSuperChic = True


    print("LHEparser: I have found ",len(root), " xml elemetns (i.e. events + header info and other stuff)")

    foundHeader     = False
    foundWeightInfo = False
    LHE_version     = -1

    for ichild, child in enumerate(root):


        ###-- Determine LHE version --###
        #if ichild == 0:
        #    if child.tag   == '<LesHouchesEvents version="1.0">':
        #        LHE_version = 1
        #    elif child.tag == '<LesHouchesEvents version="2.0">':
        #        LHE_version = 1
        #    elif child.tag == '<LesHouchesEvents version="3.0">':
        #        LHE_version = 3
        #    elif child.tag == '<LesHouchesEvents>':
        #        print("LHEparser: WARNING: there is no version infomation for this LHE file. Some features may not work properly")
        #    else:
        #        print("LHEparser: FATAL: The first line of an LHE file must be an LHE xml element")
        #        print(child.tag)
        #        sys.exit()


        ###-- All LHE files should have an init element --###

        ###-- Store all the relevant header info --###
        if child.tag == "header":

            foundHeader = True
            Headerlist = convertElementToList(child)

            if (args.generator) == None:
                print("LHEparser: Attempting to determine generator from header info")
                for line in enumerate(Headerlist):

                    isPythia    = checkGenerator(line, "Pythia")
                    isHerwig    = checkGenerator(line, "Herwig")
                    isMadGraph  = checkGenerator(line, "MadGraph")
                    isPowheg    = checkGenerator(line, "Powheg")
                    isSuperChic = checkGenerator(line, "SuperChic")


        if ichild > 10 and not foundHeader:
            print("LHEparser: Looks like this LHE doesn't contain a header element, trying to determine metadata from early lines.")

            for tempichild, tempchild in enumerate(root):

                if tempichild > 50:
                    break

                templist = convertElementToList(tempchild)

                for line in templist:

                    isPythia    = checkGenerator(line, "Pythia")
                    isHerwig    = checkGenerator(line, "Herwig")
                    isMadGraph  = checkGenerator(line, "MadGraph")
                    isPowheg    = checkGenerator(line, "Powheg")
                    isSuperChic = checkGenerator(line, "SuperChic")


        if child.tag == "event":

            ###--- Get the different lines --###

            LHEparticlelist = convertElementToList(child)

            weights = {}

            weight_string = LHEparticlelist[0].split()
            weights.update({'nominal': float(weight_string[2])})

            LHEparticlelist.pop(0)

            for thing in child[0]:
                if thing.tag == "totfact" or thing.tag == "rscale":
                    weights.update({thing.tag: thing.text})

            if len(child) > 1:
                for counter, thing in enumerate(child[1]):
                    if "wgt" in thing.tag:
                        weights.update({thing.tag + "_" + str(counter): thing.text})


            particlelist = []
            for iLHEp, LHEp in enumerate(LHEparticlelist):

                ###--- cleanup the particles ---###
                if "pdf" in LHEp: #Skip lines with pdf info, need to store this
                    continue 
                if "#aMCatNLO" in LHEp: #Skip weird lines
                    continue

                LHEp = LHEp.strip()
                LHEp = LHEp.split()
                if LHEp == []:
                    continue

                try: 
                    LHEp = list(map(int, LHEp[:6])) + list(map(float, LHEp[6:]))
                except:
                    print("Failed to convert list: ",LHEp)
                    raise AssertionError

                assert len(LHEp)==13, "Failed Assertion"

                particlelist.append( Particle(iLHEp, *LHEp) )


            eventlist.append([particlelist, weights])

    print("LHEparser: Event list set up for ",len(eventlist), "events")


    ###-- Loop through first 10 events and try to work out what process(es)
    # it is and which types of particles are present --###

    found_top    = False
    found_ttbar  = False #found 2 tops
    found_Z      = False
    found_H      = False
    found_W      = False 
    found_A      = False #Prompt photon
    found_G      = False #Gluon
    found_WbWb   = False #ttbar including off-shell
    found_b      = False #Found bs
    found_l      = False #Found leptons
    found_v      = False #Found neutrinos
    found_light  = False #Found light jet decays from Ws
    found_proton = False

    for counter, event, in enumerate(eventlist):
        if counter > 100:
            break

        ntops = 0
        nws   = 0
        nbs   = 0

        for part in event[0]:

            if abs(part.pid) == 6:
                found_top = True
                ++ntops
            if abs(part.pid) == 5:
                found_b = True
                ++nbs
            if abs(part.pid) == 24:
                found_W = True
                ++nws
            if abs(part.pid) == 25:
                found_H = True
            if abs(part.pid) == 22:
                found_A = True
            if abs(part.pid) == 23:
                found_Z = True
            if abs(part.pid) == 21:
                found_G = True
            if abs(part.pid) == 11 or \
               abs(part.pid) == 13 or \
               abs(part.pid) == 15:
                found_l = True
            if abs(part.pid) == 10 or \
               abs(part.pid) == 12 or \
               abs(part.pid) == 14:
                found_v = True
            if abs(part.pid) == 1 or \
               abs(part.pid) == 2 or \
               abs(part.pid) == 3 or \
               abs(part.pid) == 4:               
                found_light = True
            if abs(part.pid) == 2212:
                found_proton = True
        if ntops == 2:
            found_ttbar = True
        if nws > 1 and nbs > 1:
            found_WbWb = True        


    ###-- Setup output file --###

    gROOT.cd()
    outfile = TFile(args.outfile, "recreate")
    outtree = TTree(args.outtree, args.outtree)
    gROOT.cd()

    branches = []

    ###-- These branches are always written
    b_totfact      = BookBranch("w_totfact",  '/F', outtree, branches)
    b_rscale       = BookBranch("w_rscale",   '/F', outtree, branches)    
    b_weights      = BookBranch("w_weights",  '/F', outtree, branches)

    b_init_1_pt    = BookBranch('in_1_pt',    '/F', outtree, branches)    
    b_init_1_pz    = BookBranch('in_1_pz',    '/F', outtree, branches)
    b_init_1_pdgid = BookBranch('in_1_pdgid', '/F', outtree, branches)
    b_init_1_xi    = BookBranch('in_1_xi',    '/F', outtree, branches)
    b_init_2_pt    = BookBranch('in_2_pt',    '/F', outtree, branches)   
    b_init_2_pz    = BookBranch('in_2_pz',    '/F', outtree, branches)
    b_init_2_pdgid = BookBranch('in_2_pdgid', '/F', outtree, branches)
    b_init_2_xi    = BookBranch('in_2_xi',    '/F', outtree, branches)
    b_init_px      = BookBranch('in_px',      '/F', outtree, branches)    
    b_init_py      = BookBranch('in_py',      '/F', outtree, branches)        
    b_init_pz      = BookBranch('in_pz',      '/F', outtree, branches)
    b_init_pt      = BookBranch('in_pt',      '/F', outtree, branches)        
    b_init_eta     = BookBranch('in_eta',     '/F', outtree, branches)            
    b_init_y       = BookBranch('in_y',       '/F', outtree, branches)                
    b_init_phi     = BookBranch('in_phi',     '/F', outtree, branches)                
    b_init_e       = BookBranch('in_e',       '/F', outtree, branches)                    
    b_init_m       = BookBranch('in_m',       '/F', outtree, branches)

    if found_proton:
        b_p_n     = BookBranch('p_n',     '/I', outtree, branches)
        b_p_pt    = BookBranch('p_pt',    '/F', outtree, branches)
        b_p_eta   = BookBranch('p_eta',   '/F', outtree, branches)
        b_p_phi   = BookBranch('p_phi',   '/F', outtree, branches)
        b_p_e     = BookBranch('p_e',     '/F', outtree, branches)
        b_p_m     = BookBranch('p_m',     '/F', outtree, branches)
        b_p_y     = BookBranch('p_y',     '/F', outtree, branches)
        b_p_pz    = BookBranch('p_pz',    '/F', outtree, branches)
        b_p_xi    = BookBranch('p_xi',    '/F', outtree, branches)        

    ###-- Anything to do with top quarks --###
    if found_top:
        b_t_n     = BookBranch('t_n',     '/I', outtree, branches)
        b_t_pt    = BookBranch('t_pt',    '/F', outtree, branches)
        b_t_eta   = BookBranch('t_eta',   '/F', outtree, branches)
        b_t_phi   = BookBranch('t_phi',   '/F', outtree, branches)
        b_t_e     = BookBranch('t_e',     '/F', outtree, branches)
        b_t_m     = BookBranch('t_m',     '/F', outtree, branches)
        b_t_y     = BookBranch('t_y',     '/F', outtree, branches)
        b_t_pdgid = BookBranch('t_pdgid', '/F', outtree, branches)        
        #if found_ttbar:
        b_top_index  = BookBranch('top_index',  '/F', outtree, branches)
        b_tbar_index = BookBranch('tbar_index', '/F', outtree, branches)
        b_ttbar_pt   = BookBranch('ttbar_pt',   '/F', outtree, branches)
        b_ttbar_eta  = BookBranch('ttbar_eta',  '/F', outtree, branches)
        b_ttbar_phi  = BookBranch('ttbar_phi',  '/F', outtree, branches)
        b_ttbar_e    = BookBranch('ttbar_e',    '/F', outtree, branches)
        b_ttbar_m    = BookBranch('ttbar_m',    '/F', outtree, branches)
        b_ttbar_y    = BookBranch('ttbar_y',    '/F', outtree, branches)

    ###-- Anything to do with b quarks --###
    if found_b:
        b_b_n     = BookBranch('b_n',     '/I', outtree, branches)    
        b_b_pt    = BookBranch('b_pt',    '/F', outtree, branches)
        b_b_eta   = BookBranch('b_eta',   '/F', outtree, branches)
        b_b_phi   = BookBranch('b_phi',   '/F', outtree, branches)
        b_b_e     = BookBranch('b_e',     '/F', outtree, branches)
        b_b_m     = BookBranch('b_m',     '/F', outtree, branches)
        b_b_pdgid = BookBranch('b_pdgid', '/F', outtree, branches)
        if found_top:
            b_b_par_t_index = BookBranch("b_b_par_t_index", '/F', outtree, branches)

    ###-- Anything to do with Ws (including those from ttW) --###
    if found_W:
        b_W_n     = BookBranch('W_n',     '/I', outtree, branches)    
        b_W_pt    = BookBranch('W_pt',    '/F', outtree, branches)
        b_W_eta   = BookBranch('W_eta',   '/F', outtree, branches)
        b_W_phi   = BookBranch('W_phi',   '/F', outtree, branches)
        b_W_e     = BookBranch('W_e',     '/F', outtree, branches)
        b_W_m     = BookBranch('W_m',     '/F', outtree, branches)       
        b_W_pdgid = BookBranch('W_pdgid', '/F', outtree, branches)               
        if found_top:
            b_W_par_t_index = BookBranch("b_W_par_t_index", '/F', outtree, branches)

    ###-- Anything to do with leptons --###
    if found_l:
        b_l_n     = BookBranch('l_n',     '/I', outtree, branches)    
        b_l_pt    = BookBranch('l_pt',    '/F', outtree, branches)
        b_l_eta   = BookBranch('l_eta',   '/F', outtree, branches)
        b_l_phi   = BookBranch('l_phi',   '/F', outtree, branches)
        b_l_e     = BookBranch('l_e',     '/F', outtree, branches)
        b_l_m     = BookBranch('l_m',     '/F', outtree, branches)  
        b_l_pdgid = BookBranch('l_pdgid', '/F', outtree, branches)  
        if found_top:
            b_l_par_t_index = BookBranch("b_l_par_t_index", '/F', outtree, branches)
            b_l_par_W_index = BookBranch("b_l_par_W_index", '/F', outtree, branches) 
    if found_v:
        b_v_n     = BookBranch('v_n',     '/I', outtree, branches)    
        b_v_pt    = BookBranch('v_pt',    '/F', outtree, branches)
        b_v_eta   = BookBranch('v_eta',   '/F', outtree, branches)
        b_v_phi   = BookBranch('v_phi',   '/F', outtree, branches)
        b_v_e     = BookBranch('v_e',     '/F', outtree, branches)
        b_v_m     = BookBranch('v_m',     '/F', outtree, branches) 
        b_v_pdgid = BookBranch('v_pdgid', '/F', outtree, branches)                
        if found_top:
            b_v_par_t_index = BookBranch("b_v_par_t_index", '/F', outtree, branches)
            b_v_par_W_index = BookBranch("b_v_par_W_index", '/F', outtree, branches) 

    if found_light:
        b_j_n     = BookBranch('j_n',     '/I', outtree, branches)    
        b_j_pt    = BookBranch('j_pt',    '/F', outtree, branches)
        b_j_eta   = BookBranch('j_eta',   '/F', outtree, branches)
        b_j_phi   = BookBranch('j_phi',   '/F', outtree, branches)
        b_j_e     = BookBranch('j_e',     '/F', outtree, branches)
        b_j_m     = BookBranch('j_m',     '/F', outtree, branches) 
        b_j_pdgid = BookBranch('j_pdgid', '/F', outtree, branches) 

    ###-- Anything to do with additional bosons --###
    if found_H:
        b_H_n   = BookBranch('H_n',   '/I', outtree, branches)    
        b_H_pt  = BookBranch('H_pt',  '/F', outtree, branches)
        b_H_eta = BookBranch('H_eta', '/F', outtree, branches)
        b_H_phi = BookBranch('H_phi', '/F', outtree, branches)
        b_H_e   = BookBranch('H_e',   '/F', outtree, branches)
        b_H_m   = BookBranch('H_m',   '/F', outtree, branches)
    if found_Z:
        b_Z_n   = BookBranch('Z_n',   '/I', outtree, branches)    
        b_Z_pt  = BookBranch('Z_pt',  '/F', outtree, branches)
        b_Z_eta = BookBranch('Z_eta', '/F', outtree, branches)
        b_Z_phi = BookBranch('Z_phi', '/F', outtree, branches)
        b_Z_e   = BookBranch('Z_e',   '/F', outtree, branches)
        b_Z_m   = BookBranch('Z_m',   '/F', outtree, branches)
    if found_A:
        b_A_n   = BookBranch('A_n',   '/I', outtree, branches)    
        b_A_pt  = BookBranch('A_pt',  '/F', outtree, branches)
        b_A_eta = BookBranch('A_eta', '/F', outtree, branches)
        b_A_phi = BookBranch('A_phi', '/F', outtree, branches)
        b_A_e   = BookBranch('A_e',   '/F', outtree, branches)
        b_A_m   = BookBranch('A_m',   '/F', outtree, branches)
    if found_G:
        b_G_n   = BookBranch('G_n',   '/I', outtree, branches)    
        b_G_pt  = BookBranch('G_pt',  '/F', outtree, branches)
        b_G_eta = BookBranch('G_eta', '/F', outtree, branches)
        b_G_phi = BookBranch('G_phi', '/F', outtree, branches)
        b_G_e   = BookBranch('G_e',   '/F', outtree, branches)
        b_G_m   = BookBranch('G_m',   '/F', outtree, branches)

    ###-- Now loop over events --###

    for counter, event, in enumerate(eventlist):

        if counter % 1000 == 0:
            print("LHEparser: Event number ",counter,"out of",len(eventlist))

        ResetBranches(branches)

        init_1 = TLorentzVector()
        init_2 = TLorentzVector()
        init   = TLorentzVector()
    
        ###-- This assumes the first two particles are the incoming partons, 
        #     which is usually correct for LHE v2 and v3 type files.
        #     We can explicitly check this by requiring a high eta

        if abs(event[0][0].eta) > 7 and abs(event[0][1].eta) > 7 and (event[0][0].eta)*(event[0][1].eta) < 0:
            init_1.SetPtEtaPhiM(event[0][0].pt, event[0][0].eta, event[0][0].phi, event[0][0].m)
            init_2.SetPtEtaPhiM(event[0][1].pt, event[0][1].eta, event[0][1].phi, event[0][1].m)
            init = init_1 + init_2
        else:
            print("LHEparser: Couldn't determine the initial partons")
            ### We have found what looks like two very forward objects with 
       
        beam_energy = args.cme/2.

        for key in event[1]:
            if key=="totfact":
                b_totfact.push_back(float(event[1]['totfact']))
            elif key=="rscale":
                b_rscale.push_back(float((event[1]['rscale'].split())[-1]))
            else:
                b_weights.push_back(float(event[1][key]))

        b_init_1_pt.push_back(    abs(event[0][0].pt))            
        b_init_1_pz.push_back(    abs(event[0][0].pz))    
        b_init_1_pdgid.push_back( event[0][0].pid)    
        b_init_1_xi.push_back(    abs(event[0][0].pz)/beam_energy)
        b_init_2_pz.push_back(    abs(event[0][1].pz))    
        b_init_2_pt.push_back(    abs(event[0][1].pt))            
        b_init_2_pdgid.push_back( event[0][1].pid)
        b_init_2_xi.push_back(    abs(event[0][1].pz)/beam_energy)    


        ###-- Loop through particles in the event --###
        for p in event[0]:

            #p.Print()

            if abs(p.pid) > 10000: ##Skip weird doccumentation things
                continue
            elif abs(p.pid) == 2212:
                b_p_pt.push_back(p.pt)
                b_p_eta.push_back(p.eta)
                b_p_phi.push_back(p.phi)
                b_p_e.push_back(p.e)
                b_p_m.push_back(p.m)
                b_p_y.push_back(p.y)
                b_p_pz.push_back(p.pz)
                b_p_xi.push_back(1.- (abs(p.pz)/beam_energy))
            elif abs(p.pid) == 6:
                b_t_pt.push_back(p.pt)
                b_t_eta.push_back(p.eta)
                b_t_phi.push_back(p.phi)
                b_t_e.push_back(p.e)
                b_t_m.push_back(p.m)
                b_t_y.push_back(p.y)
                b_t_pdgid.push_back(p.pid)
            elif abs(p.pid) == 5:
                b_b_pt.push_back(p.pt)
                b_b_eta.push_back(p.eta)
                b_b_phi.push_back(p.phi)
                b_b_e.push_back(p.e)
                b_b_m.push_back(p.m)
                b_b_pdgid.push_back(p.pid)
            elif abs(p.pid) == 24:
                b_W_pt.push_back(p.pt)
                b_W_eta.push_back(p.eta)
                b_W_phi.push_back(p.phi)
                b_W_e.push_back(p.e)
                b_W_m.push_back(p.m)
                b_W_pdgid.push_back(p.pid)
            elif abs(p.pid) == 11 or abs(p.pid) == 13 or abs(p.pid) == 15:
                b_l_pt.push_back(p.pt)
                b_l_eta.push_back(p.eta)
                b_l_phi.push_back(p.phi)
                b_l_e.push_back(p.e)
                b_l_m.push_back(p.m)
                b_l_pdgid.push_back(p.pid)
            elif abs(p.pid) == 12 or abs(p.pid) == 14 or abs(p.pid) == 16:
                b_v_pt.push_back(p.pt)
                b_v_eta.push_back(p.eta)
                b_v_phi.push_back(p.phi)
                b_v_e.push_back(p.e)
                b_v_m.push_back(p.m)
                b_v_pdgid.push_back(p.pid)
            elif abs(p.pid) == 1 or abs(p.pid) == 2 or abs(p.pid) == 3 or abs(p.pid) == 4:
                b_j_pt.push_back(p.pt)
                b_j_eta.push_back(p.eta)
                b_j_phi.push_back(p.phi)
                b_j_e.push_back(p.e)
                b_j_m.push_back(p.m)
                b_j_pdgid.push_back(p.pid)
            elif abs(p.pid) == 25:
                b_H_pt.push_back(p.pt)
                b_H_eta.push_back(p.eta)
                b_H_phi.push_back(p.phi)
                b_H_e.push_back(p.e)
                b_H_m.push_back(p.m)
            elif abs(p.pid) == 23:
                b_Z_pt.push_back(p.pt)
                b_Z_eta.push_back(p.eta)
                b_Z_phi.push_back(p.phi)
                b_Z_e.push_back(p.e)
                b_Z_m.push_back(p.m)
            elif abs(p.pid) == 22:
                b_A_pt.push_back(p.pt)
                b_A_eta.push_back(p.eta)
                b_A_phi.push_back(p.phi)
                b_A_e.push_back(p.e)
                b_A_m.push_back(p.m)
            elif abs(p.pid) == 21:
                b_G_pt.push_back(p.pt)
                b_G_eta.push_back(p.eta)
                b_G_phi.push_back(p.phi)
                b_G_e.push_back(p.e)
                b_G_m.push_back(p.m)
            elif abs(p.pid) == 990:
                #pomeron
                continue
            elif abs(p.pid) == 93:
                #parton system, ignore
                b_init_px.push_back(  p.px)                
                b_init_py.push_back(  p.py)                                
                b_init_pz.push_back(  p.pz)                                                
                b_init_pt.push_back(  p.pt)                                                                
                b_init_eta.push_back( p.eta)
                b_init_y.push_back(   p.y)                
                b_init_phi.push_back( p.phi)                                
                b_init_e.push_back(   p.e)                                                
                b_init_m.push_back(   p.m)
                #continue 
            else:
                if not abs(p.pid) == 990:
                    print("LHEparser: WARNING - Particle",p.pid,"wasn't assigned to anything!")


        if found_proton:
            b_p_n.push_back(b_p_pt.size())
        if found_top:
            b_t_n.push_back(b_t_pt.size())
            if b_t_pt.size() == 2:
                t1 = TLorentzVector()
                t2 = TLorentzVector()
                t1.SetPtEtaPhiM(b_t_pt[0], b_t_eta[0], b_t_phi[0], b_t_m[0])
                t2.SetPtEtaPhiM(b_t_pt[1], b_t_eta[1], b_t_phi[1], b_t_m[1])                
                ttbar = t1 + t2
                b_top_index.push_back(-1)  
                b_tbar_index.push_back(-1)     
                b_ttbar_pt.push_back(ttbar.Pt())
                b_ttbar_eta.push_back(ttbar.Eta()) 
                b_ttbar_phi.push_back(ttbar.Phi())
                b_ttbar_e.push_back(ttbar.E())    
                b_ttbar_m.push_back(ttbar.M())    
                b_ttbar_y.push_back(ttbar.Rapidity())  
        if found_b:
            b_b_n.push_back(b_b_pt.size())
        if found_W:
            b_W_n.push_back(b_W_pt.size())
        if found_l:
            b_l_n.push_back(b_l_pt.size())
        if found_v:
            b_v_n.push_back(b_v_pt.size())        
        if found_light:
            b_j_n.push_back(b_j_pt.size())                
        if found_H:
            b_H_n.push_back(b_H_pt.size())                
        if found_Z:
            b_Z_n.push_back(b_Z_pt.size())                
        if found_A:
            b_A_n.push_back(b_A_pt.size())
        if found_G:
            b_G_n.push_back(b_G_pt.size())


        ###-- Do ttbar --###


        outtree.Fill()
    
    print("LHEparser: Finished looping through events, writing file")

    # write the tree into the output file and close the file
    outfile.Write()
    outfile.Close()

if __name__ == "__main__":
    main()