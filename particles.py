from math import log, tan, acos, pi, copysign


class Particle(object):

    """
    Class of generic particles (LHE or HepMC)
    """

    def __init__(self, index, pid, status, mother1, mother2, color1, color2,
                 px, py, pz, e, mass, f1, f2, prodvertex=None, decayvertex=None):
        self.index       = index
        self.pid         = pid
        self.status      = status
        self.mother1     = mother1
        self.mother2     = mother2
        self.color1      = color1
        self.color2      = color2
        self.px          = px
        self.py          = py
        self.pz          = pz
        self.e           = e
        self.m           = mass
        self.f1          = f1
        self.f2          = f2
        self.prodvertex  = prodvertex
        self.decayvertex = decayvertex
        self.p2          = self.px**2 + self.py**2 + self.pz**2
        self.p           = self.p2 ** 0.5
        self.pt          = (self.px**2 + self.py**2)**0.5
        self.theta       = acos(self.pz / self.p)
        try:
            self.phi = copysign(acos(self.px / self.pt), self.py)
        except ZeroDivisionError:
            self.phi = 0.
        try:
            self.eta = -log(tan(self.theta / 2.))
        except ValueError:
            self.eta = copysign(9999.0, self.pz / self.p)
        try:
            self.y   = 0.5 * log((self.e + self.pz) / (self.e - self.pz))
        except (ZeroDivisionError, ValueError):
            self.y   = 0

    def print(self):
        print("Particle:     ", self.pid)
        print("Status:       ", self.status)
        print("Px,Py,Pz,e:   ", round(self.px,6), round(self.py  ,6), round(self.pz,  6), round(self.e, 6))
        print("Pt,Eta,Phi,m: ", round(self.pt,6), round(self.eta ,6), round(self.phi, 6), round(self.m, 6))

    def delta_r(self, p):
        deta = self.eta - p.eta
        dphi = self.phi - p.phi
        if dphi > pi:
            dphi -= pi
        if dphi < -pi:
            dphi += pi
        return (deta**2. + dphi**2.)**.5

    def set_production_vertex(self, v):
        self.prodvertex = v

    def set_decay_vertex(self, v):
        self.decayvertex = v

    

def deltaR(a, b):
    deta = a.eta - b.eta
    dphi = a.phi - b.phi
    if dphi > pi:
        dphi -= pi
    if dphi < -pi:
        dphi += pi
    return (deta**2. + dphi**2.)**.5
